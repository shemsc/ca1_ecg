"""
CA1 ECG analysis

Student name: Sheila McCartney
"""

import sys
import numpy as np
import matplotlib.pyplot as plt


class ECGSignal:

    def load_signal(signal_file, annot_file):
        signal_array=[]
        assumption_array=[]
        signal_array=np.genfromtxt(signal_file, delimiter=",",skip_header=2)
        assumption_array=np.genfromtxt(annot_file, usecols=(0,3), skip_header=1)
        maxvalue=max(signal_array[:,1])
        minvalue=min(signal_array[:,1])
        datalength=len(signal_array[:,1])
        timelength=signal_array[-1,0]-signal_array[:1,0:1]
        sample_rate=datalength/timelength
        print("Sampling Rate: %d Hz"  % sample_rate)
        print("Data Length: %d samples" % datalength)
        print("Time Length: %d seconds" % timelength)
        print("Max Value: %f V" % maxvalue)
        print("Min Value: %f V" % minvalue)
        print("Number of Expert RWaves %d: " % len(assumption_array))
        return signal_array, assumption_array

    def plot_array(signal,annotation,found_r):
        plt.figure()
        plt.plot(signal[:,0], signal[:,1], annotation[:,0],annotation[:,1], 'ro', found_r[:,0],found_r[:,1], 'go')
        plt.title("ECG Signal")
        plt.ylabel('signal voltage')
        plt.xlabel('time')
        plt.legend(["Input", "Expert Heartbeat", "Prog Heartbeat"], loc='center left', bbox_to_anchor=(1, 0.5))
        plt.grid()
        plt.show()
        return

    def find_qrs(signal):
        datalength=len(signal[:,1])
        timelength=signal[-1,0]-signal[:1,0:1]
        sample_rate=datalength/timelength
        r_threshold = .5 * (max(signal[:,1]))  
     
        #assume heartbeat occurs at maximum of rwave for simplicity
        #set a threshold as 50% of maximum to eliminate p waves
        #find local maximum AND greater than threshold
        rwave_index = [i for i in range(1, datalength-1) 
                        if signal[i-1,1] < signal[i,1] > signal [i+1,1] 
                        and signal[i,1] >= r_threshold]
        rwave_array = np.array([signal[rwave,:] for rwave in rwave_index])
        print("Number of Program RWaves found %d: "% len(rwave_index))
        return rwave_array
        
    def compare_findings(array1,array2):
        my_signal_array = array1
        annote_signal_array = array2

        rwave_count = 0
        for i in range(0, len(annote_signal_array)):
            for j in range(0, len(my_signal_array)):
                if annote_signal_array[i,0] == my_signal_array[j,0]:
                    rwave_count = rwave_count + 1
                    i=i+1
                else: j=j+1
        print("Number of RWaves matched: %d" % rwave_count)
        rwave_unmatched = len(my_signal_array)-rwave_count 
        print("Number of RWaves not matched: %d" % rwave_unmatched)
        rwave_missed = len(annote_signal_array)-len(my_signal_array)
        print("Number of RWaves missed: %d" % rwave_missed)
        if len(my_signal_array)-len(annote_signal_array) > 0:
            rwave_added=len(my_signal_array)-len(annote_signal_array)
        else: rwave_added=0
        print("Number of RWaves added: %d" % rwave_added)
        
        return
 
    def rwave_annot(array1,array2):
        rwave_annot_array=[]
        my_signal_array = array1
        annote_signal_array = array2
        xwave=0

        for i in range(0, len(annote_signal_array)):
            for j in range(0, len(my_signal_array)):
                if annote_signal_array[i,0] == my_signal_array[j,0]:
                    rwave_annot_array.append(my_signal_array[j,0:2])
                else: j=j+1
        rwave_annot_array=np.vstack(rwave_annot_array)
        return rwave_annot_array


if __name__=='__main__':
    signal_array,annotation_array=ECGSignal.load_signal(sys.argv[1]+'_signals.txt',sys.argv[1]+'_annotations.txt')
    rwave_result_array=ECGSignal.find_qrs(signal_array)
    ECGSignal.compare_findings(rwave_result_array,annotation_array)
    annot_result=ECGSignal.rwave_annot(signal_array,annotation_array)
    ECGSignal.plot_array(signal_array, annot_result, rwave_result_array)
    
