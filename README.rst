CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): Sheila McCartney

Sortable name (no spaces, surname then firstname): McCartney_Sheila

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

Firstly, I would add more robust error handling.  Secondly, with regard to the ECG itself, I would try and improve the Heartbeat matches.  Alough I did have a good success rate with regard to the number of Rwaves, the matches in terms of seconds and voltage in file2 and file3 were zero.  In a review of the data, file3 for example had annotated rwaves with negative values, and both file2 and file3 the annotation identifies the Rwave below the actual peak of the wave.  I assumed a heartbeat at the peak of the peak, which gave a decent result with regard to the number of Rwaves, but was inconsistent on a more detail match.  I did some research for a more sophisticatd algorithm however in reading some of the papers I found that there are many different algorithms, quite complex and I did not find any consensus with regard to the best.  


Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)

ECG signals differ widely and as a result, identificaiton of Rwaves is not trivial.  


Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

A great way to get better acquainted with Python and in particular, how to use pyplot. 


